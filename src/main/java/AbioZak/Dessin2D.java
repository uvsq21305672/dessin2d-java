package AbioZak;

import java.io.Serializable;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

public class Dessin2D implements Serializable {
	
	private int x = 15;
	private int y = 5;
	public Forme matrice[][] = new Forme[x][y];

	public Dessin2D() {
		for (int i = 0; i < x ; i++)
		{
			for (int j = 0; j < y ; j++)
			{
				matrice[i][j] = new Forme("-",i,j);
			}
		}
	}

	public void ajouter(String type,int x,int y)
	{
		matrice[x][y] = new Forme(type,x,y);
	}
	
	public void afficher()
	{
		for (int i = 0; i < y ; i++)
		{
			for (int j = 0; j < x ; j++)
			{
				System.out.print(matrice[j][i].afficher());
			}
			System.out.println();
		}
	}
	
	public void sauvegarder()
	{
		ObjectOutputStream oos;
		try{
			oos = new ObjectOutputStream(new BufferedOutputStream(new FileOutputStream(new File("fic.txt"))));
			oos.writeObject(this);
			oos.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();}
	}
	
	
	public void charger()
	{
		ObjectInputStream ois;
		try{
			ois = new ObjectInputStream(new BufferedInputStream(new FileInputStream(new File("fic.txt"))));
			try {
				((Dessin2D)ois.readObject()).afficher();
			} catch (ClassNotFoundException e) {
				e.printStackTrace();}
			ois.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();}
	}
}
