package AbioZak;
import java.io.Serializable;

public class Forme implements Serializable  {
	
	public String forme;
	public int x;
	public int y;
	
	public Forme(String form){
		this.forme = form;
		this.x = 0;
		this.y = 0;
	}
	
	public Forme(String form, int x, int y){
		this.forme = form;
		this.x = x;
		this.y = y;
	}

	public String afficher() {
		return forme+"  ";
	}
	
	public int Getcoordoneex() {
		return x;
	}
	public int Getcoordoneey() {
		return y;
	}
	
	public void deplacer(int xx,int yy) {
		x = x + xx;
		y = y + yy;
	}
}
