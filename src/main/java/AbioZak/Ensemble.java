package AbioZak;
import java.util.LinkedList;

public class Ensemble {
	
	private LinkedList<Forme> ensemble;
	
	public Ensemble() {
		ensemble = new LinkedList<Forme>();
	}
	
	public void ajouter(Forme f){
		ensemble.add(f);
	}
	
	public LinkedList<Forme> ToString(){
		return ensemble;
	}
//public LinkedList ensemble;
	

	public void deplacer(Dessin2D D,int xx, int yy) {
		for (int i = 0; i < ensemble.size() ; i++)
		{
			int x = ( ensemble.get(i)).x;
			int y = ( ensemble.get(i)).y;
			String forme = ( ensemble.get(i)).forme;
			D.matrice[x][y] = new Forme("-");
			( ensemble.get(i)).deplacer(xx,yy);
			D.matrice[x+xx][y+yy] = new Forme(forme);
		}
	}
}
