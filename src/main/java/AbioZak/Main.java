package AbioZak;

public class Main 
{
    public static void main( String[] args )
    {
		System.out.println("\n\t\t\t **********DESSIN2D*********");
		System.out.println();
    	try {
        Dessin2D D = new Dessin2D();
        
        D.ajouter("R",10,3);
        D.ajouter("T",5,1);
        D.ajouter("C",12,2);
        D.ajouter("L",4,0);
        Ensemble E = new Ensemble();
        E.ajouter(D.matrice[10][3]);
        E.ajouter(D.matrice[5][1]);
        D.afficher();
        
        System.out.println();
        System.out.println();
        
        E.deplacer(D,2,1);
        D.afficher();
        System.out.println();
    
        D.sauvegarder();
        
        //D.charger();
    	}
    	catch(ArrayIndexOutOfBoundsException e) {
    		 System.out.println("Une des formes d�passe les limites de la matrice !");
    	}
    }
}
