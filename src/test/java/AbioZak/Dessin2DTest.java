package AbioZak;
import static org.junit.Assert.*;

import org.junit.Test;

public class Dessin2DTest {

	@Test
	public void Formetest() {
		Forme f = new Forme("T",5,7);
		f.deplacer(2, 3);
		assertEquals(new Integer(f.Getcoordoneex()),new Integer(7));
		assertEquals(new Integer(f.Getcoordoneey()),new Integer(10));	
	}

	@Test
	public void Ensembletest() {
		Dessin2D d = new Dessin2D();
		Ensemble e = new Ensemble();
		Forme f = new Forme("T",5,1);
		Forme ff = new Forme("R",11,3);
		e.ajouter(f);
		e.ajouter(ff);
		e.deplacer(d, 3, 1);
		assertEquals(new Integer(f.Getcoordoneex()),new Integer(8));
		assertEquals(new Integer(ff.Getcoordoneex()),new Integer(14));
		
	}
}
